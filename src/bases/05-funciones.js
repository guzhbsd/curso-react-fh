const saludar = function(nombre){
    return `Hola ${nombre}`;
}

const saludar2 = (nombre)=>{
    return `Hola ${nombre}`;
}

const saludar3 = (nombre) => `Hola ${nombre}`;
const saludar4 = () => `Hola mundo`;

console.log(saludar('Goku'));
console.log(saludar2('Vegeta'));
console.log(saludar3('Trunks'));
console.log(saludar4());

const getUser = ()=> ({
    uid: 'AA012',
    username: 'guzh'
})

console.log(getUser());

const getUsuarioActivo = (nombre)=>({
        uid:'ABC111',
        username:nombre
})

const usuarioActivo = getUsuarioActivo('Gustavo');
console.log(usuarioActivo);