// const getImagenPromesa = ()=> new Promise(resolve => resolve('https://asdasdsa.com'));
// getImagenPromesa().then(console.log);

const getImagen = async ()=>{
  try {
    const apiKey = 'B77gDy0YQBD2JVJ1NKng1ni1sCBMmBtz';
    const resp = await fetch(`https://api.giphy.com/v1/gifs/random?api_key=${apiKey}`);
    const {data} = await resp.json();
    const {url} = data.images.original;
    const img = document.createElement('img');
    img.src = url;
    document.body.append(img);
  } catch (error) {
    console.error(error);
  }
}
getImagen();
