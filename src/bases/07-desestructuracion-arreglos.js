const personajes = ['Goku','Vegeta','Trunks'];
const [p1] = personajes;
const [,p2] = personajes;
const [, ,p3] = personajes;

console.log(p1);
console.log(p2);
console.log(p3);

const retornaArreglo = ()=>{
    return ['ABC', 123];
}

const arr = retornaArreglo();
const [letras, numeros] = retornaArreglo();
console.log(arr);
console.log(letras, numeros);

const usaEstado = ( valor ) => {
    return [ valor, ()=> { console.log( 'Hola mundo' ) } ];
}

const [nombre, setNombre] = usaEstado('Goku');
console.log(nombre);
setNombre();
