const persona = {
    nombre: 'Tony',
    apellido:'Stark',
    edad: 45,
    clave: 'Iron man',
};

// extrae el nombre del objeto persona
const {nombre} = persona;
// extrae el nombre del objeto persona pero en una variable nombre2
const {nombre:nombre2} = persona;

console.log(nombre);
console.log(nombre2);

const retornaPersona = ({clave, nombre,edad, rango='Capitan'})=>{
    // const {nombre, edad, clave} = usuario;
    // console.log(nombre, edad, clave);
    return (
        {
            nombreClave: clave,
            anios: edad,
            latlng: {
                lat: 14.255,
                lng: -12.044,
            }
        }
    )
}

const {nombreClave, anios, latlng} = retornaPersona(persona);
const {latlng:{lat, lng}} = retornaPersona(persona);
console.log(nombreClave, anios);
console.log(latlng);
console.log(lat, lng);