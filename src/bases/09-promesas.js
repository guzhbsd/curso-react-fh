/**
 * las promesas son asincronas
 * */

import { getHeroeById } from "./bases/08-importaciones";

// const promesa = new Promise((resolve, reject)=>{
//     setTimeout(()=>{
//         // console.log('2 segundos despues');
//         const heroe = getHeroeById(2);
//         console.log(heroe);
//         resolve(heroe);
//     }, 2000)
// });

// promesa.then((heroe)=>{
//     console.log('then de la promesa');
// })
// .catch(err => console.warn(err));

const getHeroeByIdAsync = (id) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      const heroe = getHeroeById(id);
      if (heroe){
        resolve(heroe);
      }else{
        reject('No se ha encontrado el heroe');
      }
    }, 2000);
  });
};

getHeroeByIdAsync(10)
  /* .then((heroe) => console.log("Heroe", heroe)) */
  .then(console.log)
  /* .catch((mensaje)=> console.log(mensaje)); */
  .catch(console.log);
