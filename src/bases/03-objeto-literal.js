const persona = {
    nombre: 'Tony',
    apellido: 'Stark',
    edad: 45,
    direccion: {
        ciudad: 'New York',
        zip: 45678,
        lat: 10.22233,
        lng: 14.04444
    }
};

console.log(persona);

// esto copia la referencia de persona a persona2
const persona2 = persona;
const persona3 = {...persona};
persona2.nombre = 'Peter';

console.log('Persona:', persona);
console.log('Persona2:', persona2);
console.log('Persona3:', persona3);
